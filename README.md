# Chromium Screenshot

## Status

[![Codacy Badge](https://app.codacy.com/project/badge/Grade/6496ff0d000a4a639b92422fb33eb6a2)](https://app.codacy.com/gl/complex-space/chromium-screenshot/dashboard?utm_source=gl&utm_medium=referral&utm_content=&utm_campaign=Badge_grade)
[![Codacy Badge](https://app.codacy.com/project/badge/Coverage/6496ff0d000a4a639b92422fb33eb6a2)](https://app.codacy.com/gl/complex-space/chromium-screenshot/dashboard?utm_source=gl&utm_medium=referral&utm_content=&utm_campaign=Badge_coverage)


## Documentation

Package documentation is deployed to [GitLab Pages](https://complex-space.gitlab.io/chromium-screenshot).


## Dependencies

This package requires Node.js 8.x or later; modules required by the package are
documented in the [package.json][] file.


## Code Quality

To run the unit tests and generate a coverage report in `/coverage` with [Mocha][]
and [Istanbul][] run `npm install` followed by `npm test` at the command line.

To run [ESLint][] run `npm install` followed by `npm run lint` at the command line.


## Building documentation

Generating the docs requires [Python](https://www.python.org/) to be installed, tested with v3.12.  With the Python binary in the path, run, install [MkDocs](https://www.mkdocs.org/) if not present:

```bash
➜  mkdir venv               
➜  python -m venv venv 
➜  source venv/bin/activate
➜  which python
/path/to/venv/bin/python
➜  python -V
Python 3.12.3
➜  pip install mkdocs mkdocs-material
```

Then to generate the docs run:
```bash
➜  npm i
➜  npm run docs
```
to generate static files in `./site`.




[ESLint]: https://eslint.org/
[Istanbul]: https://istanbul.js.org/
[MkDocs]: https://www.mkdocs.org/
[Mocha]: https://mochajs.org/
[package.json]: ./package.json
